
import os
import sys
import unittest
import coverage

cov = coverage.Coverage()


if __name__ == '__main__':

    cov.start()
    test_suite = unittest.TestLoader().discover(os.path.join(os.path.dirname(os.path.abspath(__file__)), "."),
                                                pattern='test_*.py')
    result = unittest.TextTestRunner(buffer=True).run(test_suite)
    cov.stop()
    cov.save()
    cov.report()
    cov.html_report()
    sys.exit(not result.wasSuccessful())
