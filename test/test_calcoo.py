#!/usr/bin/python3
# -*- coding: utf-8 -*-
import unittest
from src import calcoo


class TestCalculadora(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        test_value1 = 1
        test_value2 = 6
        cls.test_calculadora = calcoo.Calculadora(test_value1, test_value2)


    def test_suma(self):
        """Should sum values"""
        result = self.test_calculadora.suma()
        expected_result = 7
        self.assertEqual(result, expected_result)


    def test_resta(self):
        """Should substract values"""
        result = self.test_calculadora.resta()
        expected_result = -5
        self.assertEqual(result, expected_result)


if __name__ == "__main__":
    unittest.main()
