#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest
from src import calc

class TestCalc(unittest.TestCase):

    def test_plus(self):
        """ Should sum the operands. They have to be ints """
        result = calc.plus(4,2)
        expected_result = 6
        self.assertEqual(result, expected_result)


    def test_minus(self):
        """ Should substract the operands """
        result = calc.minus(4, 2)
        expected_result = 2
        self.assertEqual(result, expected_result)

if __name__ == "__main__":
    unittest.main()
