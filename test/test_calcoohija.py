
import unittest
from src import calcoohija


class TestCalculadora(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.test_value1 = 5
        cls.test_value2 = 1
        cls.test_value0 = 0
        cls.test_calculadora = calcoohija.CalculadoraHija(cls.test_value1, cls.test_value2)
        cls.test_calc_divide_zero = calcoohija.CalculadoraHija(cls.test_value1, cls.test_value0)

    def test_suma(self):
        """Should sum values"""
        result = self.test_calculadora.suma()
        expected_result = 6
        self.assertEqual(result, expected_result)

    def test_resta(self):
        """Should substract values"""
        result = self.test_calculadora.resta()
        expected_result = 4
        self.assertEqual(result, expected_result)
    
    def test_multiplica(self):
        """Should multiply values"""
        result = self.test_calculadora.multiplica()
        expected_result = 5
        self.assertEqual(result, expected_result)

    def test_divide(self):
        """Should divide values"""
        result = self.test_calculadora.divide()
        expected_result = 5
        self.assertEqual(result, expected_result)
        
        with self.assertRaises(SystemExit) as exc:
            self.test_calc_divide_zero.divide()
            self.assertEqual("No se dividir por cero", str(exc.exception))


if __name__ == "__main__":
    unittest.main()
